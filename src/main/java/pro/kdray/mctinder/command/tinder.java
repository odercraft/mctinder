package pro.kdray.mctinder.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pro.kdray.mctinder.data.TinderBio;
import pro.kdray.mctinder.data.TinderUser;
import pro.kdray.mctinder.gui.ProfileGUI;
import pro.kdray.mctinder.mctinder;

public class tinder implements CommandExecutor {

    private static void showUsage(CommandSender sender) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c------------------ \n" +
                "&r&f/tinder - Shows this message \n" +
                "/tinder bio - View your own bio \n" +
                "/tinder bio <key> [value] - View/set each value for your bio \n" +
                "/tinder like [player] - Lets you like a player \n" +
                "/tinder dislike [player] - Lets you dislike a player \n" +
                "/tinder match - Finds a match for you \n" +
                "&c------------------"));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length > 0) {
            switch (strings[0]) {
                case "bio":
                    if (!commandSender.hasPermission("tinder.bio")) {
                        commandSender.sendMessage("You don't have permission :/");
                        return true;
                    }
                    return this.bio((Player) commandSender, strings);
                case "like":
                    if (!commandSender.hasPermission("tinder.like")) {
                        commandSender.sendMessage("You don't have permission :/");
                        return true;
                    }
                    if (strings.length < 2) {
                        showUsage(commandSender);
                        return true;
                    }
                    Player player = Bukkit.getPlayer(strings[1]);
                    if (player == null) {
                        commandSender.sendMessage("That player cannot be found");
                        return true;
                    }
                    if (mctinder.getInstance().getUser((Player) commandSender).like(mctinder.getInstance().getUser(player)))
                        commandSender.sendMessage("You liked " + player.getDisplayName());
                    return true;
                case "dislike":
                    if (!commandSender.hasPermission("tinder.dislike")) {
                        commandSender.sendMessage("You don't have permission :/");
                        return true;
                    }
                    if (strings.length < 2) {
                        showUsage(commandSender);
                        return true;
                    }
                    player = Bukkit.getPlayer(strings[1]);
                    if (player == null) {
                        commandSender.sendMessage("That player cannot be found");
                        return true;
                    }
                    mctinder.getInstance().getUser((Player) commandSender).dislike(mctinder.getInstance().getUser(player));
                    commandSender.sendMessage("You disliked " + player.getDisplayName());
                    return true;
                case "match":
                    if (!commandSender.hasPermission("tinder.match")) {
                        commandSender.sendMessage("You don't have permission :/");
                        return true;
                    }
                    TinderUser tu = mctinder.getInstance().getUser((Player) commandSender);
                    TinderUser user = tu.findMatch();
                    if (user == null) {
                        commandSender.sendMessage(ChatColor.DARK_RED + "Couldn't find a match at this time, try again later...");
                        return true;
                    }
                    new ProfileGUI((Player) commandSender, user, false);
                case "setDate":
                    if (!commandSender.hasPermission("tinder.setDate")) {
                        commandSender.sendMessage("You don't have permission :/");
                        return true;
                    }
                    if (!(commandSender instanceof Player))
                        return true;
                    player = (Player) commandSender;
                    if (player.getInventory().getItemInMainHand().getType() == Material.AIR) {
                        commandSender.sendMessage(ChatColor.DARK_RED + "You must be holding an item to run this command");
                        return true;
                    }
                    FileConfiguration dateLocations = mctinder.getDateLocations();
                    ItemStack item = player.getInventory().getItemInMainHand();
                    String location = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                    if (location == null)
                        location = ChatColor.stripColor(item.getItemMeta().getLocalizedName());
                    if (location == null)
                        location = item.getType().name();
                    if (dateLocations.get(location) != null) {
                        commandSender.sendMessage(ChatColor.DARK_RED + "This date already exists (The name is the item's name)");
                        return true;
                    }
                    dateLocations.set(location + ".Item", item);
                    dateLocations.set(location + ".world", player.getLocation().getWorld().getName());
                    dateLocations.set(location + ".X", player.getLocation().getX());
                    dateLocations.set(location + ".Y", player.getLocation().getY());
                    dateLocations.set(location + ".Z", player.getLocation().getZ());
                    dateLocations.set(location + ".YAW", player.getLocation().getYaw());
                    dateLocations.set(location + ".PITCH", player.getLocation().getPitch());
                    mctinder.saveDateLocations(dateLocations);
                    commandSender.sendMessage(ChatColor.RED + "Date location set as " + location);
                    return true;
            }
        }
        showUsage(commandSender);
        return true;
    }

    private boolean bio(Player player, String[] args) {
        TinderUser target = mctinder.getInstance().getUser(player);
        TinderBio bio = target.getBio();
        if (args.length == 1) {
            new ProfileGUI(player, target);
            return true;
        } else if (args.length == 2) {
            switch (args[1]) {
                case "age":
                    player.sendMessage("Your age is: " + bio.getAge());
                    return true;
                case "description":
                    player.sendMessage("Your description is: " + bio.getBio());
                    return true;
                case "playtime":
                    player.sendMessage("You've been playing for: " + bio.getPlaytime());
                    return true;
                case "gender":
                    player.sendMessage("Your gender is: " + bio.getGender());
                    return true;
                case "name":
                    player.sendMessage("Your name is: " + bio.getName());
                    return true;
                default:
                    if (Bukkit.getPlayer(args[1]) == null) {
                        showUsage(player);
                        return true;
                    }
                    new ProfileGUI(player, mctinder.getInstance().getUser(Bukkit.getPlayer(args[1])));
                    return true;
            }
        } else if (args.length >= 3) {
            switch (args[1]) {
                case "age":
                    int age = Integer.valueOf(args[2]);
                    if (age >= 0 && age <= 100) {
                        bio.setAge(age);
                        player.sendMessage("Set age to " + age);
                    } else {
                        player.sendMessage(age + " is not a valid age between 0 and 100.");
                    }
                    return true;
                case "description":
                    StringBuilder builder = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        builder.append(args[i]).append(" ");
                    }
                    bio.setBio(builder.toString());
                    player.sendMessage("Set bio to \"" + builder.toString() + "\"");
                    return true;
            }
        }
        showUsage(player);
        return true;
    }
}
