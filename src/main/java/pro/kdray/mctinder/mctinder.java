package pro.kdray.mctinder;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import pro.kdray.mctinder.command.tinder;
import pro.kdray.mctinder.data.TinderUser;
import pro.kdray.mctinder.storage.Datasource;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

public class mctinder extends JavaPlugin {

    private static mctinder instance;
    private Datasource datasource;
    private HashMap<UUID, TinderUser> users = new HashMap<>();

    public static mctinder getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        //Enable stuff
        instance = this;
        this.datasource = new Datasource("jdbc:h2:" + this.getDataFolder().getAbsolutePath() + "/database");
        this.getCommand("tinder").setExecutor(new tinder());
    }

    @Override
    public void onDisable() {
        //Disable stuff
        this.datasource.disconnect();
    }

    public Datasource getDatasource() {
        return this.datasource;
    }

    public TinderUser getUser(UUID uuid) {
        if (users.containsKey(uuid)) {
            return users.get(uuid);
        } else {
            TinderUser user = new TinderUser(Bukkit.getOfflinePlayer(uuid), this.datasource);
            users.put(uuid, user);
            return user;
        }
    }

    public TinderUser getUser(OfflinePlayer player) {
        UUID uuid = player.getUniqueId();
        if (users.containsKey(uuid)) {
            return users.get(uuid);
        } else {
            TinderUser user = new TinderUser(player, this.datasource);
            users.put(uuid, user);
            return user;
        }
    }

    public static FileConfiguration getDateLocations() {
        return YamlConfiguration.loadConfiguration(new File(instance.getDataFolder(), "dateLocations.yml"));
    }

    public static void saveDateLocations(FileConfiguration config) {
        try {
            config.save(new File(instance.getDataFolder(), "dateLocations.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
