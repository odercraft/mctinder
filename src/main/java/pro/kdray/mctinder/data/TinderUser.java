package pro.kdray.mctinder.data;

import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import pro.kdray.mctinder.gui.DateGUI;
import pro.kdray.mctinder.mctinder;
import pro.kdray.mctinder.storage.Datasource;

import java.util.UUID;

public class TinderUser {

    private UUID player;
    private TinderLikes likes;
    private TinderPrefs prefs;
    private TinderBio bio;

    public TinderUser(OfflinePlayer player, Datasource datasource) {
        this.player = player.getUniqueId();
        this.likes = datasource.getTinderLikes(this.player);
        this.prefs = datasource.getTinderPrefs(this.player);
        this.bio = datasource.getTinderBio(this.player);
    }

    public TinderLikes getLikes() {
        return this.likes;
    }

    public TinderPrefs getPrefs() {
        return this.prefs;
    }

    public TinderBio getBio() {
        return this.bio;
    }

    public boolean like(TinderUser otherUser) {
        if (otherUser.player.equals(this.player)) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.DARK_RED + "YOU CANNOT LIKE YOURSELF!");
            return false;
        } else if (otherUser.getLikes().getLikes().contains(this.player) || otherUser.getBio().checkForLike(this.player)) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.RED + "Aww... you already liked them <3 " + ChatColor.DARK_RED + ChatColor.BOLD + "Too bad you can only like them once </3");
            return false;
        } else if (this.getLikes().getDislikes().contains(otherUser.player)) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.DARK_RED + "This person seems to not like you :/");
            return false;
        } else if (otherUser.getLikes().getDislikes().contains(this.player))
            otherUser.getLikes().removeDislike(this.player);
        else if (!this.getBio().canLike()) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.DARK_RED + "You've exceeded your like limit. Consider buying a rank (or a higher one) to expand this limit.");
            return false;
        }
        otherUser.onLike(this);
        return true;
    }

    public void dislike(TinderUser otherUser) {
        if (otherUser.player.equals(this.player)) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.DARK_RED + "YOU CANNOT DISLIKE YOURSELF!");
            return;
        } else if (otherUser.getLikes().getDislikes().contains(this.player)) {
            if (Bukkit.getPlayer(this.player) != null)
                Bukkit.getPlayer(this.player).sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You already dislike them... don't hate on them :/");
            return;
        }
        otherUser.getLikes().addDislike(this.player);
    }

    public TinderUser findMatch() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            TinderUser user = mctinder.getInstance().getUser(player);
            if (user.player != this.player && !user.getLikes().getLikes().contains(this.player) && !user.getLikes().getDislikes().contains(this.player) && !this.getLikes().getDislikes().contains(this.player))
                return user;
        }
        return null;
    }

    private void onMatch(TinderUser with, boolean madeMatch) {
        Player player = Bukkit.getPlayer(this.player);
        if (player == null)
            return;
        if (madeMatch) {
            new DateGUI(player, with);
            with.onMatch(this, false);
        }
        player.sendMessage(ChatColor.RED + "A match was made with you and " + with.getBio().getName() + "!");
    }

    private void onLike(TinderUser liker) {
        this.likes.addLike(liker.player);
        if (Bukkit.getPlayer(this.player) == null)
            return;
        Player player = Bukkit.getPlayer(this.player);
        if (liker.getBio().checkForLike(this.player)) {
            this.onMatch(liker, true);
        } else {
            new PendingLike(liker.getBio(), this.getBio());
            player.sendMessage(ChatColor.RED + "------------------");
            player.sendMessage(liker.bio.getName() + " has liked you, do you want to view their profile?");
            player.spigot().sendMessage(new ComponentBuilder("Yes ")
                    .color(net.md_5.bungee.api.ChatColor.RED)
                    .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tinder bio " + liker.getBio().getUsername()))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent("View profile")}))
                    .append("No")
                    .color(net.md_5.bungee.api.ChatColor.BLUE)
                    .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, ""))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent("Ignore")}))
                    .create());
            player.sendMessage(ChatColor.RED + "------------------");
        }
    }
}
