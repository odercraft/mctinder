package pro.kdray.mctinder.data;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.poweredbyhate.gender.GenderPlugin;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import pro.kdray.mctinder.Utils;
import pro.kdray.mctinder.mctinder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

public class TinderBio {

    private UUID player;
    private String bio;
    private String skin;
    private String name;
    private int age;
    private Set<PendingLike> pendingLikes = new HashSet<>();
    private Set<PendingLike> sentLikes = new HashSet<>();

    public TinderBio(UUID player, String bio, String skin, String name, int age) {
        this.player = player;
        this.bio = bio;
        this.skin = skin;
        this.name = name;
        this.age = age;
    }

    public OfflinePlayer getPlayer() {
        return Bukkit.getOfflinePlayer(this.player);
    }

    public String getUsername() {
        return Bukkit.getOfflinePlayer(this.player).getName();
    }

    public String getPlaytime() {
        Player player = Bukkit.getPlayer(this.player);
        int time = 0;
        if (player == null) {
            try {
                File stats = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/stats/" + this.player.toString() + ".json");
                if (!stats.exists()) {
                    return "???";
                }

                Scanner sc = new Scanner(stats);
                sc.useDelimiter("\\Z");
                JsonObject elem = new JsonParser().parse(sc.nextLine()).getAsJsonObject();

                if (elem.get("stat.playOneMinute") == null) {
                    return "???";
                }

                time = elem.get("stats.playOneMinute").getAsInt();

            } catch (FileNotFoundException e) {
                return "???";
            }
        } else {
            time = player.getStatistic(Statistic.PLAY_ONE_TICK);
        }

        if (time == 0) {
            return "???";
        }

        int date = time * 50;

        long seconds = date / 1000 % 60;
        long minutes = date / (60 * 1000) % 60;
        long hours = date / (60 * 60 * 1000) % 24;
        long days = date / (24 * 60 * 60 * 1000);

        return days + " d " + hours + " h " + minutes + " m " + seconds + " s";
    }

    public String getGender() {
        if (!Bukkit.getServer().getPluginManager().isPluginEnabled("Gender"))
            return "???";
        return GenderPlugin.instance.getAsylum().getGender(player);
    }

    public String getBio() {
        return bio;
    }

    public TinderBio setBio(String bio) {
        this.bio = bio;
        mctinder.getInstance().getDatasource().updateBio(this);
        return this;
    }

    public String getSkin() {
        return this.skin;
    }

    public TinderBio setSkin(String skin) { //TODO:
        this.skin = skin;
        mctinder.getInstance().getDatasource().updateBio(this);
        return this;
    }

    public String getName() {
        if (Bukkit.getPlayer(this.player) == null && this.name == null)
            return this.getUsername();
        return Bukkit.getPlayer(this.player).getDisplayName();
    }

    public TinderBio setName(String name) {
        this.name = name;
        mctinder.getInstance().getDatasource().updateBio(this);
        return this;
    }

    public int getAge() {
        return this.age;
    }

    public TinderBio setAge(int age) {
        this.age = age;
        mctinder.getInstance().getDatasource().updateBio(this);
        return this;
    }

    public TinderBio save() {
        mctinder.getInstance().getDatasource().updateBio(this);
        return this;
    }

    public void addPendingLike(PendingLike like) {
        this.pendingLikes.add(like);
    }

    public void addSentLike(PendingLike like) {
        this.sentLikes.add(like);
    }

    public void removePendingLike(PendingLike like) {
        this.pendingLikes.remove(like);
    }

    public void removeSentLike(PendingLike like) {
        this.sentLikes.remove(like);
    }

    public boolean checkForLike(UUID player) {
        for (PendingLike like : this.pendingLikes) {
            if (like.getFrom().getPlayer().getUniqueId() == player)
                return !like.isExpired();
        }
        return false;
    }

    public boolean canLike() {
        String limit = Utils.getPermissionValue("tinder.likelimit.", this.getPlayer().getPlayer());
        if (limit == null)
            return false;
        int limitN = Integer.parseInt(limit);
        return limitN >= this.sentLikes.size();
    }
}
