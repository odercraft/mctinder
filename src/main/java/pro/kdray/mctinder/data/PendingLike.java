package pro.kdray.mctinder.data;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;
import pro.kdray.mctinder.mctinder;

public class PendingLike {

    TinderBio to;
    TinderBio from;
    boolean expired = false;
    BukkitTask task;

    public PendingLike(TinderBio from, TinderBio to) {
        this.from = from;
        this.to = to;

        to.addPendingLike(this);
        from.addSentLike(this);

        this.task = Bukkit.getScheduler().runTaskLaterAsynchronously(mctinder.getInstance(), () -> {
            this.expired = true;
            this.from.removePendingLike(this);
        }, 120 * 20); //120 seconds or two minutes
        this.task = Bukkit.getScheduler().runTaskLaterAsynchronously(mctinder.getInstance(), () -> this.to.removeSentLike(this), 60 * 1 * 20); //60 minutes or one hour
    }

    public void cancel() {
        this.task.cancel();
    }

    public TinderBio getFrom() {
        return this.from;
    }

    public TinderBio getTo() {
        return this.to;
    }

    public boolean isExpired() {
        return this.expired;
    }

}
