package pro.kdray.mctinder.data;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import pro.kdray.mctinder.mctinder;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class TinderLikes {
    private UUID player;
    private Set<UUID> likes;
    private Set<UUID> dislikes;

    public TinderLikes(UUID player, Object[] likes, Object[] dislikes) {
        this.player = player;
        if (likes instanceof String[]) {
            Set<UUID> likesSet = new HashSet<>();
            for (Object string : likes) {
                likesSet.add(UUID.fromString((String) string));
            }
            this.likes = likesSet;
        } else {
            this.likes = new HashSet<>();
        }
        if (dislikes instanceof String[]) {
            Set<UUID> dislikesSet = new HashSet<>();
            for (Object string : dislikes) {
                dislikesSet.add(UUID.fromString((String) string));
            }
            this.dislikes = dislikesSet;
        } else {
            this.dislikes = new HashSet<>();
        }
    }

    public OfflinePlayer getPlayer() {
        return Bukkit.getOfflinePlayer(this.player);
    }

    public Set<UUID> getLikes() {
        return likes;
    }

    public Set<UUID> getDislikes() {
        return dislikes;
    }

    public TinderLikes addLike(UUID p) {
        this.likes.add(p);
        mctinder.getInstance().getDatasource().updateLikes(this);
        return this;
    }

    public TinderLikes addDislike(UUID p) {
        this.dislikes.add(p);
        mctinder.getInstance().getDatasource().updateLikes(this);
        return this;
    }

    public TinderLikes removeLike(UUID p) {
        this.likes.remove(p);
        mctinder.getInstance().getDatasource().updateLikes(this);
        return this;
    }

    public TinderLikes removeDislike(UUID p) {
        this.dislikes.remove(p);
        mctinder.getInstance().getDatasource().updateLikes(this);
        return this;
    }

    public TinderLikes save() {
        mctinder.getInstance().getDatasource().updateLikes(this);
        return this;
    }
}
