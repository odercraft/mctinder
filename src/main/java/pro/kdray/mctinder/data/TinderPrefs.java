package pro.kdray.mctinder.data;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import pro.kdray.mctinder.mctinder;

import java.util.UUID;

public class TinderPrefs {

    private UUID player;
    private boolean likes;
    private boolean match;

    public TinderPrefs(UUID player, boolean likes, boolean match) {
        this.player = player;
        this.likes = likes;
        this.match = match;
    }

    public OfflinePlayer getPlayer() {
        return Bukkit.getOfflinePlayer(this.player);
    }

    public boolean isLikes() {
        return likes;
    }

    public TinderPrefs setLikes(boolean likes) {
        this.likes = likes;
        mctinder.getInstance().getDatasource().updatePrefs(this);
        return this;
    }

    public boolean isMatch() {
        return match;
    }

    public TinderPrefs setMatch(boolean match) {
        this.match = match;
        mctinder.getInstance().getDatasource().updatePrefs(this);
        return this;
    }

    public TinderPrefs save() {
        mctinder.getInstance().getDatasource().updatePrefs(this);
        return this;
    }
}
