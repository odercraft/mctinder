package pro.kdray.mctinder.storage;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.h2.jdbcx.JdbcConnectionPool;
import pro.kdray.mctinder.data.TinderBio;
import pro.kdray.mctinder.data.TinderLikes;
import pro.kdray.mctinder.data.TinderPrefs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Datasource {

    private static final String TINDER_LIKES_CREATE = "CREATE TABLE IF NOT EXISTS tinderLikes (uuid VARCHAR(36), likes ARRAY, dislikes ARRAY);";
    private static final String TINDER_BIOS_CREATE = "CREATE TABLE IF NOT EXISTS tinderBios (uuid VARCHAR(36), bio TEXT, skin TEXT, name VARCHAR(32), age INT);";
    private static final String TINDER_PREFS_CREATE = "CREATE TABLE IF NOT EXISTS tinderPrefs (uuid VARCHAR(36), likes BOOL, match BOOL);";
    private static final String TINDER_LIKES_SELECT = "SELECT likes,dislikes FROM tinderLikes WHERE uuid=?;";
    private static final String TINDER_BIOS_SELECT = "SELECT bio,skin,name,age FROM tinderBios WHERE uuid=?;";
    private static final String TINDER_PREFS_SELECT = "SELECT likes,match FROM tinderPrefs WHERE uuid=?;";
    private static final String TINDER_LIKES_UPDATE = "MERGE INTO TinderLikes key(uuid) values(?,?,?);";
    private static final String TINDER_BIOS_UPDATE = "MERGE INTO TinderBios key(uuid) values(?,?,?,?,?);";
    private static final String TINDER_PREFS_UPDATE = "MERGE INTO TinderPrefs key(uuid) values(?,?,?);";
    private JdbcConnectionPool cp;

    public Datasource(String path) {
        cp = JdbcConnectionPool.create(path, "", "");
        try {
            Connection conn = cp.getConnection();
            if (conn == null)
                return;
            PreparedStatement statement = conn.prepareStatement(TINDER_LIKES_CREATE);
            statement.execute();
            statement = conn.prepareStatement(TINDER_BIOS_CREATE);
            statement.execute();
            statement = conn.prepareStatement(TINDER_PREFS_CREATE);
            statement.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public TinderLikes getTinderLikes(UUID player) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return null;
            PreparedStatement statement = conn.prepareStatement(TINDER_LIKES_SELECT);
            statement.setString(1, player.toString());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                TinderLikes result = new TinderLikes(player, (Object[]) rs.getArray("likes").getArray(), (Object[]) rs.getArray("dislikes").getArray());
                conn.close();
                return result;
            } else {
                conn.close();
                return new TinderLikes(player, new String[0], new String[0]).save();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TinderBio getTinderBio(UUID player) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return null;
            PreparedStatement statement = conn.prepareStatement(TINDER_BIOS_SELECT);
            statement.setString(1, player.toString());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                TinderBio result = new TinderBio(player, rs.getString("bio"), rs.getString("skin"), rs.getString("name"), rs.getInt("age"));
                conn.close();
                return result;
            } else {
                conn.close();
                if (Bukkit.getPlayer(player) == null)
                    return new TinderBio(player, "", "", Bukkit.getOfflinePlayer(player).getName(), 0).save();
                Player player1 = Bukkit.getPlayer(player);
                return new TinderBio(player, "", "", player1.getDisplayName(), 0).save(); //TODO: Add skin
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TinderPrefs getTinderPrefs(UUID player) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return null;
            PreparedStatement statement = conn.prepareStatement(TINDER_PREFS_SELECT);
            statement.setString(1, player.toString());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                TinderPrefs result = new TinderPrefs(player, rs.getBoolean("likes"), rs.getBoolean("match"));
                conn.close();
                return result;
            } else {
                conn.close();
                return new TinderPrefs(player, true, true).save();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateLikes(TinderLikes likes) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return;
            PreparedStatement statement = conn.prepareStatement(TINDER_LIKES_UPDATE);
            statement.setString(1, likes.getPlayer().getUniqueId().toString());
            statement.setArray(2, conn.createArrayOf("VARCHAR(36)", likes.getLikes().toArray()));
            statement.setArray(3, conn.createArrayOf("VARCHAR(36)", likes.getDislikes().toArray()));
            statement.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBio(TinderBio bio) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return;
            PreparedStatement statement = conn.prepareStatement(TINDER_BIOS_UPDATE);
            statement.setString(1, bio.getPlayer().getUniqueId().toString());
            statement.setString(2, bio.getBio());
            statement.setString(3, bio.getSkin());
            statement.setString(4, bio.getName());
            statement.setInt(5, bio.getAge());
            statement.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePrefs(TinderPrefs prefs) {
        try {
            Connection conn = this.cp.getConnection();
            if (conn == null)
                return;
            PreparedStatement statement = conn.prepareStatement(TINDER_PREFS_UPDATE);
            statement.setString(1, prefs.getPlayer().getUniqueId().toString());
            statement.setBoolean(2, prefs.isLikes());
            statement.setBoolean(3, prefs.isMatch());
            statement.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        cp.dispose();
    }
}
