package pro.kdray.mctinder.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import pro.kdray.mctinder.data.TinderUser;
import pro.kdray.mctinder.mctinder;

import java.util.HashMap;
import java.util.Map;

public class DateGUI {

    Map<Integer, Location> invSlots = new HashMap<>();
    private Player user;
    private TinderUser target;
    private Inventory gui;

    public DateGUI(Player user, TinderUser target) {
        this.user = user;
        this.target = target;
        this.gui = Bukkit.createInventory(user, 27, ChatColor.RED + "Date Selector");
        this.loadGUI(this.gui, mctinder.getDateLocations());
        Bukkit.getPluginManager().registerEvents(new InventoryHandler(), mctinder.getInstance());
        this.user.openInventory(gui);
    }

    private void loadGUI(Inventory GUI, FileConfiguration config) {
        int currentSlot = 0;
        for (String key : config.getKeys(false)) {
            Location place = new Location(Bukkit.getServer().getWorld(
                    config.getString(key + ".world")),
                    config.getDouble(key + ".X"),
                    config.getDouble(key + ".Y"),
                    config.getDouble(key + ".Z"),
                    config.getLong(key + ".YAW"),
                    config.getLong(key + ".PITCH"));
            this.invSlots.put(currentSlot, place);
            GUI.setItem(currentSlot, config.getItemStack(key + ".Item"));
            currentSlot++;
        }
    }

    private class InventoryHandler implements Listener {

        private boolean isClosed = false;


        @EventHandler
        public void onOpen(InventoryOpenEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getPlayer() != user)
                return;
            isClosed = false;
        }

        @EventHandler
        public void onClick(InventoryClickEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getWhoClicked() != user)
                return;
            e.setCancelled(true);
            if (invSlots.containsKey(e.getSlot())) {
                HandlerList.unregisterAll(this);
                isClosed = true;
                e.getWhoClicked().closeInventory();
                e.getWhoClicked().teleport(invSlots.get(e.getSlot()));
                Player targetP = Bukkit.getPlayer(target.getBio().getPlayer().getUniqueId());
                targetP.sendMessage(ChatColor.RED + "" + user.getDisplayName() + ChatColor.RED + " has liked you back. You will be teleported to the date location \'" + e.getCurrentItem().getItemMeta().getDisplayName() + "\' in 5 seconds.");
                Bukkit.getScheduler().runTaskLaterAsynchronously(mctinder.getInstance(), () -> targetP.teleport(invSlots.get(e.getSlot())), 5 * 20); //5 seconds
            }
        }

        @EventHandler
        public void onClose(InventoryCloseEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getPlayer() != user)
                return;
            if (isClosed) {
                return;
            }
            Bukkit.getScheduler().runTaskLater(mctinder.getInstance(), () -> e.getPlayer().openInventory(gui), 1L);
        }
    }
}
