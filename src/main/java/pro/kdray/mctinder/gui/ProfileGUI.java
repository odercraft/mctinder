package pro.kdray.mctinder.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import pro.kdray.mctinder.data.TinderBio;
import pro.kdray.mctinder.data.TinderUser;
import pro.kdray.mctinder.mctinder;

import java.util.ArrayList;
import java.util.List;

public class ProfileGUI {

    private static final int likeSlot = 19;
    private static final int dislikeSlot = 25;

    private Player user;
    private TinderUser target;
    private Inventory gui;
    private boolean allowClose;

    public ProfileGUI(Player user, TinderUser target, boolean allowClose) {
        this.user = user;
        this.target = target;
        this.allowClose = allowClose;
        this.gui = Bukkit.createInventory(user, 27, ChatColor.RED + target.getBio().getName() + ChatColor.RED + "'s Profile");
        //Make head
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        TinderBio bio = this.target.getBio();
        headMeta.setOwningPlayer(bio.getPlayer());
        headMeta.setDisplayName(ChatColor.RED + bio.getName());
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + "Age: " + bio.getAge());
        lore.add(ChatColor.RED + "Gender: " + bio.getGender());
        lore.add(ChatColor.RED + "Playtime: " + bio.getPlaytime());
        lore.add(ChatColor.RED + "Description: " + bio.getBio());
        headMeta.setLore(lore);
        head.setItemMeta(headMeta);
        this.gui.setItem(4, head);
        //If the person is viewing their own bio, don't show likes
        if (this.target.getBio().getPlayer().getUniqueId() == this.user.getUniqueId()) {
            this.user.openInventory(this.gui);
            Bukkit.getPluginManager().registerEvents(new SelfIHandler(), mctinder.getInstance());
            return;
        }
        //Make like item
        ItemStack likeableStack = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta likeableMeta = likeableStack.getItemMeta();
        likeableMeta.setDisplayName(ChatColor.RED + "Like");
        likeableStack.setItemMeta(likeableMeta);
        this.gui.setItem(likeSlot, likeableStack);
        //Make dislike item
        ItemStack myStack = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta myMeta = myStack.getItemMeta();
        myMeta.setDisplayName(ChatColor.BLUE + "Dislike");
        myStack.setItemMeta(myMeta);
        this.gui.setItem(dislikeSlot, myStack);
        Bukkit.getPluginManager().registerEvents(new InventoryHandler(), mctinder.getInstance());
        this.user.openInventory(this.gui);
    }

    public ProfileGUI(Player user, TinderUser target) {
        new ProfileGUI(user, target, true);
    }

    private class SelfIHandler implements Listener {
        @EventHandler
        public void onClick(InventoryClickEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getWhoClicked() != user)
                return;
            e.setCancelled(true);
        }

        @EventHandler
        public void onClose(InventoryCloseEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getPlayer() != user)
                return;
            HandlerList.unregisterAll(this);
        }
    }

    private class InventoryHandler implements Listener {

        private boolean isClosed = false;


        @EventHandler
        public void onOpen(InventoryOpenEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getPlayer() != user)
                return;
            isClosed = false;
        }

        @EventHandler
        public void onClick(InventoryClickEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getWhoClicked() != user)
                return;
            e.setCancelled(true);

            if (e.getSlot() == likeSlot) {
                this.isClosed = true;
                user.closeInventory();
                mctinder.getInstance().getUser(user).like(target);
            } else if (e.getSlot() == dislikeSlot) {
                this.isClosed = true;
                user.closeInventory();
                //mctinder.getInstance().getUser(user).dislike(target);
            }
        }

        @EventHandler
        public void onClose(InventoryCloseEvent e) {
            if (!e.getInventory().getTitle().equals(gui.getTitle()))
                return;
            if (e.getPlayer() != user)
                return;
            if (isClosed || allowClose) {
                HandlerList.unregisterAll(this);
                return;
            }
            Bukkit.getScheduler().runTaskLaterAsynchronously(mctinder.getInstance(), () -> e.getPlayer().openInventory(gui), 1L);
        }
    }
}
