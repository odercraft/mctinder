package pro.kdray.mctinder;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;
import pro.kdray.mctinder.data.TinderBio;

public class Utils {

    public static void sendBio(TinderBio bio, CommandSender sender) {
        sender.sendMessage(ChatColor.RED + "---------" + bio.getName() + "---------");
        sender.sendMessage("Description: " + bio.getBio());
        sender.sendMessage("Age: " + bio.getAge());
        sender.sendMessage("Gender: " + bio.getGender());
        sender.sendMessage("Playtime: " + bio.getPlaytime());

        StringBuilder footer = new StringBuilder();
        for (int i = 0; i < bio.getName().length() + 18; i++) {
            footer.append("-");
        }
        sender.sendMessage(ChatColor.RED + footer.toString());
    }

    public static String getPermissionValue(String node, Player player) {
        for (PermissionAttachmentInfo permissionAttachmentInfo : player.getEffectivePermissions()) {
            String permission = permissionAttachmentInfo.getPermission();
            if (permission.contains(node))
                return permission.split(node)[1];
        }
        return null;
    }
}
